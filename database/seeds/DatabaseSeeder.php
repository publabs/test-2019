<?php

use Illuminate\Database\Seeder;
use App\Spend;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$sources = [
    		'google',
    		'bing',
    		'revcontent',
    		'outbrain',
    		'taboola',
    		'facebook',
    		'gemini'
    	];

    	$start = microtime(true);

    	for($x = 0; $x < 100000; $x++) {
    		$spend = new Spend;
    		$spend->source = $sources[mt_rand(0, count($sources) - 1)];
    		$spend->spend = mt_rand(0, 99) . '.' . mt_rand(0, 99);
    		$spend->date = date('Y-m-d');
    		$spend->save();

    		if (($x % 100) == 0) {
    			$this->command->info('Seeder has created ' . $x . ' spends so far...');
    		}
    	}

    	$end = microtime(true);

    	$elapsed = $end - $start;

    	$this->command->info(sprintf("Took %.6f seconds to create spends.", $elapsed));
    }
}
