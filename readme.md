## Pub Labs inc. Test 2019

To get started, simply clone the repository, set up your database and add credentials to the .env file. Then, run migrations and finally data seeder.

This will create 100,000 entries in the spends table, with random source and spend value. This should take about 2 minutes to run the seeder.

In order to compile those spends per source, run the spend compile command:

``php artisan spend:compile``

This will output total spend for each source.

#### Test #1

Please optimize the spend compile command so it takes less time to return total spend for each source.

#### Test #2 (Bonus)

Please optimize the database seeder so it takes less time to generate those 100,000 random entries.

When you're done, send modified PHP files (zipped) to <results@publabs.com>.

Thanks.

Happy coding!
