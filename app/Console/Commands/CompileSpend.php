<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Spend;

class CompileSpend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spend:compile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile spend per source.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = microtime(true);

        $spends = Spend::all();
        $sources = [];
        $count = 0;

        foreach($spends as $spend) {
            if (!isset($sources[$spend->source])) {
                $sources[$spend->source] = 0;
            }

            $count++;

            if (($count % 100) == 0) {
                $this->info('Compiled ' . $count . ' spends so far...');
            }

            $sources[$spend->source] += $spend->spend;
        }

        $end = microtime(true);

        $elapsed = $end - $start;

        $this->info(sprintf("Took %.6f seconds to compile spends.", $elapsed));

        foreach($sources as $source => $spend) {
            $this->info('Source "' . $source . '" spend: $' . $spend);
        }
    }
}
